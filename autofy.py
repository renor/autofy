# Author: renor (hialvaro)
# License: GPL

import os
import re
import subprocess
import string
import sys
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

#EDIT THIS VARIABLES!#############################
# Do not remove '' or ""
client_id = '<client_id>'
client_secret = '<client_secret>'
mediadir="."
###################################################

#Initializations
artist=''
album=''
song=''
result=''

client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager) #spotify object to access API

def ask_info():
    artist=input("Type the Artist name: ")
    album=input("Type the Album name: ")
    url=input("Paste the Spotify album url: ")


def dw_to_dest(destdir, url, fr, what):
    if what == "a":
        if fr==2: 
            os.makedirs(destdir)
        subprocess.run(["spotdl", "-b", url])

        txtfile=album.lower()
        txtfile=txtfile.replace(" ", "-")
        txtfile=re.sub('[,\'"@.#]', '', txtfile)
        txtfile=txtfile+".txt"
    
        try:
            subprocess.run(["spotdl", "--list", txtfile, "-f", destdir])
            print("Album download was successful!")
        except ValueError:
            print("Something went wrong! :(")
    else: #Song
        if fr==2:
            os.makedirs(destdir)
        try:
            subprocess.run(["spotdl", "-f", destdir, "--song", str(song)+" - "+str(artist)])
            print("Song download was successful!")
        except ValueError:
            print("There was an error downloading the son :(")
    return

def chk_dir_and_ask(destdir):
    exists=os.path.exists(destdir)
    if exists:
        option=2
        print("Path already exists, hence album may do so.")
        print("[1] Continue anyways.")
        print("[2] Change destination path. (Default)")
        print("[3] Cancel.")
        option=input("Choose an option ")
        
        if option==2: # New destination dir.
            destdir=input("Input your absolute destination dir ('/path/to/dir'): ")
            chk_dir_and_ask(destdir)
        elif option==3:
            return option  
        else:
            return option
    else:
        return 2 # this is not option 2, just a code to guide the program.

def get_and_select_albums(uri):
    sp_albums=sp.artist_albums(uri, album_type='album')
    album_links=[]

    for i in range(len(sp_albums['items'])):
        print('[' + str(i) + '] ' + sp_albums['items'][i]['name'] + ": " +sp_albums['items'][i]['external_urls']['spotify'])
        album_links.append(sp_albums['items'][i]['external_urls']['spotify'])
        
    print("[x] The album is not here (enter manually)")
    sel=input("Select an album from the list: ")
    if sel=='x':
        return False
    else:
        global album
        album=sp_albums['items'][int(sel)]['name']
        return album_links[int(sel)]

def options():
    print("What do you want to download? ")
    print("[1] Whole Album")
    print("[2] Single Song")
    opt = input("Choose an option: ")
    try:
        return int(opt)
    except ValueError:
        print("Please select a valid option!")
        options()

def find_artist_albums(artist_input):
    #Searches for an artist, if not found gives list to select. Returns artist_uri.
    found = 0
    i = 0
    list=[]
    try: #Tries if the input artist was correct
        while found == 0:
            artist_found= "["+str(i)+"] "+result['artists']['items'][i]['name']
            list.append(artist_found)
            if(result['artists']['items'][i]['name']==artist_input):
                artist_uri = result['artists']['items'][i]['uri']
                found = 1
                return artist_uri
            i = i+1
    except IndexError:
        found=2 # This means artist was not found.

    if found == 2:
        print("Artist " + artist_input + " not found!")
        art = "[x] The artist is not listed here"
        list.append(art)
        for artist_item in list:
            print(artist_item)
        option=input("Did you mean any of the listed? ")
        if option!='x':
            global artist
            artist=result['artists']['items'][int(option)]['name']
            return(result['artists']['items'][int(option)]['uri'])
        else:
            sys.exit(0)
        return artist_uri

def main_album():
    global artist
    global album
    global result
    artist=input("Type the Artist name: ")
    result = sp.search(artist, type='artist') #search query
    uri=find_artist_albums(artist)
    url=get_and_select_albums(uri)

    destdir=mediadir+'/'+artist+'/'+album
    if url:
        print("Album " + album + " by "+artist+" will be stored at "+destdir)
        isok=input("Is it Ok? [Y/N]")
    else:
        album=input("Type the album name: ")
        url=input("Paste spotify url: ")
        print("Album " + album + " by "+artist+" will be stored at "+destdir)
        isok=input("Is it Ok? [Y/N]")

    destdir=mediadir+"/"+artist+"/"+album
    if(isok=="Y"):
        exists=chk_dir_and_ask(destdir)    
        if int(exists)==2:
            dw_to_dest(destdir, url, 2, "a")
        elif int(exists)==1:
            dw_to_dest(destdir, url, 1, "a")
        else:
            print("Operation was cancelled. Bye!")
            sys.exit(0)
    else:
        main_album()
    return

def main_song():
    global artist
    global song
    
    artist=input("Type the Artist name: ")
    song=input("Type the song name: ")
    destdir=mediadir+'/'+artist+"/"+song
    
    print(song + " by "+artist+" will be stored at "+destdir)
    isok=input("Is it Ok? [Y/N]")
    
    if(isok=="Y"):
        exists=chk_dir_and_ask(destdir)    
        if int(exists)==2:
            dw_to_dest(destdir, "none", 2, "s")
        elif int(exists)==1:
            dw_to_dest(destdir, "none", 1, "s")
        else:
            print("Operation was cancelled. Bye!")
            sys.exit(0)
    else:
        main_song()
    return


def usage():
    print("Please, you should select a valid option from the fields")
    main()
    return

def main():
    action=options()
    if action==1:
        main_album()
    elif action==2:
        main_song()
    else:
        usage()

main()

