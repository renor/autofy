# Version 0.0.2 Alpha
* You can now download a Single Track or a Whole Album
* Improved artist search: It now gives you suggerences about artists if you mistake, or artist was not found.
* Improved album search (Previous version would not give always correct albums)
* Improved errors handling
* Major bug fixes

# Version 0.0.1 Alpha

* Download Spotify Albums to .mp3 searching by Author.
* Import Spotify metadata to tracks.
* Easy guided CLI interface.
* Manually enter the Album data to download.