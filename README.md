# 🎵  autofy - v0.0.2a
Autofy is a Python CLI automatic downloader for Spotify Albums and song tracks. You can download albums searching by **Artist name**, manually entering the **Spotify url** or searching a track by **name - artist** and it gets you all the .mp3 files and the metadata of the album in them (cover, track, artist, album, year...)

Autofy is based on the [spotdl](https://pypi.org/project/spotdl/) downloader and the [Spotipy](https://spotipy.readthedocs.io/) python library. Autofy combines this two tools together to get a easy way to download your favourite albums.

## ⚠️ Disclaimer
Downloading copyright songs may be illegal in your country. This tool is for educational purposes only and was created only to show how Spotify's API can be exploited to download music from YouTube. Please always support the artists by buying their music.


## 📁 Installation 

1. [Install Python3](https://www.python.org/downloads/source/)

2. Install spotdl with pip: <br>
`pip install spotdl`

3. Install Spotipy with pip: <br>
`pip install spotipy`

4. Clone this repository (or copy the code from autopy.py): <br>
`git clone https://github.com/hialvaro/autofy.git` <br>
`cd autofy`

## ⚠️ Important: Preparation ⚠️
You will need to set up some variables for Autofy to work as desired. If you open the "autofy.py" with your favourite editor, you will see three variables that are needed to change. To do so, you have to create a Spotify App and get your tokens from there:
Go to [the developers dashboard](https://developer.spotify.com/dashboard), create an app with any name you want and get the tokens from there. There you will be able to obtain the tokens for `client_id` and `client_secret` variables.

Last variable is `mediadir`, the directory where the downloads will be placed, this one is optional and by default is the same directory that autofy is being executed.



**Now you are set to run autofy:** <br>
`python autofy.py`

## 📗 Usage 
When you run Autofy you will be immediatelly prompted to input an Artist. When you do so, it will search for the artist's albums on Spotify and give you a list of them. Then you may select the one you are looking for (number based selection).

If your album does not appear, or the search was not right, you can choose to enter manually the album data. It will prompt you with Album Name and Spotify's Album URL. Then it will download it.

If you're curious about how the download is done, check out [spotdl](https://pypi.org/project/spotdl/) project.

## 💙 Future features ️
* ~~Search by artist.~~ v0.0.2
* Search by song.
* ~~Download just one song.~~ v0.0.2
* Get user Spotify statistics.
